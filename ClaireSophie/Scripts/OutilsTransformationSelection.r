# Projet Big Data Management and Analysis
# 
# Equipe projet : Gauthier CASTRO - Claire-Sophie MARONI
# Date : janvier 2019
# Source base de donnees : # https://www.data.gouv.fr/fr/datasets/fichier-des-prenoms-edition-2017/
# 
# Ensemble d'exemples de manipulation des colonnes et des lignes des données

library(dplyr)

#################### EXEMPLES DE TRAITEMENT SELECTIONS.... SUR LES COLONNES OU LES LIGNES ################

source("LectureDonnees.r")


### Transformation des colonnes de la BDD ####-------------------------------------------------------------

# Création d'une colonne
demo <- data %>%
  mutate(project_team = "Gauthier C. et CSM", cumulcol=paste(sexe,preusuel))

print(demo[1:3,])
# summary(data)

# sélection de colonne(s)
tmp <- data %>%
  select(preusuel,nombre)
print(tmp[1:3,])

# suppression d'une colonne
demo <- demo %>%
  select(-project_team,- cumulcol)
print(demo[1:3,])



### Tri, sélection, cumule sur les lignes......  #####-----------------------------------

# NB : 
# - avec filter : on conserve par défaut l'ensemble des colonnes de la table de départ
# - si il y a summarize : il faut mentionner toutes les variables que l'on veut garder dans la table (cf. exemple)

### Selectionne les lignes du tableau selon leur position
slice(data, 1:5)

### Cumule les frequences par prénom
data_sum_freq <- data %>% 
  group_by(preusuel)  %>% 
  summarise(total=sum(nombre)) %>%
  ungroup()  # Facultatif?


### Compte le nombre d'apparition d'un prénom dans la liste
res <- data %>%         
  count(preusuel, sort = TRUE)

### Selectionne les lignes pour un prénom exact demandé 
name <- "PIERRE"
res <- data %>%
  filter(data$preusuel==name)

# Encore plus simple
res <- data %>%
  filter(preusuel==name)

res <- data %>%
  filter(str_detect(preusuel,name))

### Selectionne les lignes pour lesquelles le prénom contient un motif donné  (en debut, milieu ou fin de prénom)
pattern ="AA"
res <- data %>%
  filter(grepl(pattern,data$preusuel,fixed=TRUE))

# ou 
library(stringr)
res <- data %>%
  filter(str_detect(preusuel,pattern))

# Exemple : recherche des prénoms composés
pattern ="-"
res <- data %>%
  filter(grepl(pattern,data$preusuel,fixed=TRUE))

### Selectionne les lignes pour un prenom et une année donnés
name <- "PIERRE"
year <- 1990
res <- data %>%
  filter(preusuel==name)%>%
  filter(annais == "1990")

# ou en groupant les critères
res2 <- data %>%
  filter(preusuel==name & annais == "1990")


### Decompte le nombre de " PIERRE" ou autre prénom choisi,  par département, 
# sur l'ensemble des années de la base : 1900,2019
name <- "PIERRE"
res <- data %>%
  filter(preusuel==name)%>%
  group_by(dpt) %>% 
  summarise(total=sum(nombre)) 

### Decompte le nombre de " PIERRE", par département, entre deux années choisies 
name <- "PIERRE"
res <- data %>%
  filter(preusuel==name ) %>%
  filter((2015 < annais) & (annais< 2017)) %>%
  group_by(dpt) %>% 
  summarise(preusuel=preusuel[1],total=sum(nombre))  
# preusuel=preusuel[1], permet de garder la colonne prenom

# Autres exemples de synthaxe

# filter(is_blank == FALSE, is_separator == FALSE) %>%
#   select(-is_blank, -is_separator) %>%                  
#   mutate(text = str_remove_all(text, "[\\r\\n\\t]+"))    # crée une nouvelle colonne


#### Brouillon #####
# lignes <- match("PIERRE",data$preusuel)
# #lignes <- (data$preusuel %in% "PIERRE" )
# data$preusuel[1:10]
# dim(data)
# print(data$preusuel[lignes[1:10]])
# lignes[1]

# essai <- "A" %in% c("A","B","A"); print(essai)
# essai <-  c("A","B","A") %in% "A" ; print(essai)
# 
# essai <- match("A",c("A","B","A"));print(essai)
# essai <- match(c("A","B","A"),"A");print(essai)

# res_year <- data %>%
#   filter(grepl('AARON',data$preusuel,fixed=TRUE))%>%
#   filter(annais == "1990")%>%
#   group_by(dpt) %>% 
#   summarise(total=sum(nombre)) 
# 
# essai <- data$preusuel[grepl('AARON',data$preusuel,fixed=TRUE)]
# essai2 <- data[grepl('AARON',data$preusuel,fixed=TRUE),]
# essai2_annee <- essai2[essai2$annais == "1990",]

