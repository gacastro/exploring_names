# Projet Big Data Management and Analysis
# 
# Equipe projet : Gauthier CASTRO - Claire-Sophie MARONI
# Date : janvier 2019
# Source base de donnees : # https://www.data.gouv.fr/fr/datasets/fichier-des-prenoms-edition-2017/
# 
# Lecture Données - Mise en forme

library(dplyr)


#### Lecture des données #####

### Lecture en gérant l'encoding
data <- read.csv("../DataSets/dpt2017.txt", sep="\t", stringsAsFactors = FALSE,encoding="UTF-8")
# data <- read.csv("dpt2017.txt", sep="\t", stringsAsFactors = FALSE)

# Renomme la première colonne dont le nom est un peut bizarre : X.U.FEFF.sexe
data <- rename(data, sexe= X.U.FEFF.sexe)

